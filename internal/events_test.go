package internal

import (
	"log"
	"testing"
	"time"
	"user-service/pkg"
	"user-service/pkg/postgresql"

	"github.com/stretchr/testify/assert"
)

func TestNewEventsInApp(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)

	eventsHandler := NewEventsHandler(repo)
	var requestEvents []pkg.ScheduledEvent
	event1 := pkg.ScheduledEvent{ID: "test1", FromHours: 9, FromMinutes: 0, ToHours: 10, ToMinutes: 0, Name: "Тестовое событие 1"}
	// TODO: вернуть после реализации сортировки. Пока-что не ясно, как писать юнит тесты с несколькими элементами массива,
	// т.к. при преобразовании из мапы порядок всегда разный
	// event2 := pkg.ScheduledEvent{ID: "test2", FromHours: 10, FromMinutes: 0, ToHours: 11, ToMinutes: 0, Name: "Тестовое событие 2"}
	requestEvents = append(requestEvents, event1)

	updateDate := time.Now()
	repo.On("GetUserEvents", 10).Return(make([]pkg.ScheduledEvent, 0), nil)
	repo.On("UpdateUserEvents", 10, requestEvents, updateDate).Return(nil)
	syncedEvents, err := eventsHandler.Sync(requestEvents, 10, time.Now(), updateDate)
	if err != nil {
		log.Print("Got error while testing Sync: ", err)
		t.FailNow()
	}
	assert.Equal(t, requestEvents, syncedEvents, "input and response events are different")
}

func TestOutdatedApp(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)

	eventsHandler := NewEventsHandler(repo)
	requestEvents := make([]pkg.ScheduledEvent, 0)
	updateDate := time.Now()

	dbEvents := make([]pkg.ScheduledEvent, 0)
	event1 := pkg.ScheduledEvent{ID: "test1", FromHours: 9, FromMinutes: 0, ToHours: 10, ToMinutes: 0, Name: "Тестовое событие 1", UpdateDate: updateDate}
	// TODO: вернуть после реализации сортировки. Пока-что не ясно, как писать юнит тесты с несколькими элементами массива,
	// т.к. при преобразовании из мапы порядок всегда разный
	// event2 := pkg.ScheduledEvent{ID: "test2", FromHours: 10, FromMinutes: 0, ToHours: 11, ToMinutes: 0, Name: "Тестовое событие 2", UpdateDate: updateDate}
	dbEvents = append(dbEvents, event1)

	lastSyncDate := time.Date(2009, 11, 17, 20, 34, 58, 651387237, time.UTC)
	repo.On("GetUserEvents", 10).Return(dbEvents, nil)
	repo.On("UpdateUserEvents", 10, dbEvents, updateDate).Return(nil)
	syncedEvents, err := eventsHandler.Sync(requestEvents, 10, lastSyncDate, updateDate)
	if err != nil {
		log.Print("Got error while testing Sync: ", err)
		t.FailNow()
	}
	assert.Equal(t, dbEvents, syncedEvents, "input and response events are different")
}

func TestEventsDeletedInApp(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)

	eventsHandler := NewEventsHandler(repo)
	requestEvents := make([]pkg.ScheduledEvent, 0)
	updateDate := time.Now()
	lastSyncDate := time.Date(2009, 11, 17, 20, 34, 58, 651387237, time.UTC)

	dbEvents := make([]pkg.ScheduledEvent, 0)
	event1 := pkg.ScheduledEvent{ID: "test1", FromHours: 9, FromMinutes: 0, ToHours: 10, ToMinutes: 0, Name: "Тестовое событие 1", UpdateDate: lastSyncDate}
	// TODO: вернуть после реализации сортировки. Пока-что не ясно, как писать юнит тесты с несколькими элементами массива,
	// т.к. при преобразовании из мапы порядок всегда разный
	// event2 := pkg.ScheduledEvent{ID: "test2", FromHours: 10, FromMinutes: 0, ToHours: 11, ToMinutes: 0, Name: "Тестовое событие 2", UpdateDate: lastSyncDate}
	dbEvents = append(dbEvents, event1)

	repo.On("GetUserEvents", 10).Return(dbEvents, nil)
	updatedEvents := make([]pkg.ScheduledEvent, 0)
	updatedEvent1 := pkg.ScheduledEvent{ID: "test1", FromHours: 9, FromMinutes: 0, ToHours: 10, ToMinutes: 0, Name: "Тестовое событие 1", UpdateDate: lastSyncDate, Deleted: true}
	// TODO: вернуть после реализации сортировки. Пока-что не ясно, как писать юнит тесты с несколькими элементами массива,
	// т.к. при преобразовании из мапы порядок всегда разный
	// updatedEvent2 := pkg.ScheduledEvent{ID: "test2", FromHours: 10, FromMinutes: 0, ToHours: 11, ToMinutes: 0, Name: "Тестовое событие 2", UpdateDate: lastSyncDate, Deleted: true}
	updatedEvents = append(updatedEvents, updatedEvent1)
	repo.On("UpdateUserEvents", 10, updatedEvents, updateDate).Return(nil)
	syncedEvents, err := eventsHandler.Sync(requestEvents, 10, lastSyncDate, updateDate)
	if err != nil {
		log.Print("Got error while testing Sync: ", err)
		t.FailNow()
	}
	assert.Equal(t, requestEvents, syncedEvents, "input and response events are different")
}

func TestEventUpdatedInApp(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)

	eventsHandler := NewEventsHandler(repo)
	updateDate := time.Now()
	lastSyncDate := time.Date(2009, 11, 17, 20, 34, 58, 651387237, time.UTC)

	requestEvents := make([]pkg.ScheduledEvent, 0)
	// TODO: вернуть после реализации сортировки. Пока-что не ясно, как писать юнит тесты с несколькими элементами массива,
	// т.к. при преобразовании из мапы порядок всегда разный
	// requestEvent1 := pkg.ScheduledEvent{ID: "test1", FromHours: 9, FromMinutes: 0, ToHours: 10, ToMinutes: 0, Name: "Тестовое событие 1"}
	requestEvent2 := pkg.ScheduledEvent{ID: "test2", FromHours: 11, FromMinutes: 0, ToHours: 12, ToMinutes: 0, Name: "Тестовое событие 2"}
	requestEvents = append(requestEvents, requestEvent2)

	dbEvents := make([]pkg.ScheduledEvent, 0)
	// TODO: вернуть после реализации сортировки. Пока-что не ясно, как писать юнит тесты с несколькими элементами массива,
	// т.к. при преобразовании из мапы порядок всегда разный
	// event1 := pkg.ScheduledEvent{ID: "test1", FromHours: 9, FromMinutes: 0, ToHours: 10, ToMinutes: 0, Name: "Тестовое событие 1", UpdateDate: lastSyncDate}
	event2 := pkg.ScheduledEvent{ID: "test2", FromHours: 10, FromMinutes: 0, ToHours: 11, ToMinutes: 0, Name: "Тестовое событие 2", UpdateDate: lastSyncDate}
	dbEvents = append(dbEvents, event2)

	repo.On("GetUserEvents", 10).Return(dbEvents, nil)
	updatedEvents := make([]pkg.ScheduledEvent, 0)
	// TODO: вернуть после реализации сортировки. Пока-что не ясно, как писать юнит тесты с несколькими элементами массива,
	// т.к. при преобразовании из мапы порядок всегда разный
	// updatedEvent1 := pkg.ScheduledEvent{ID: "test1", FromHours: 9, FromMinutes: 0, ToHours: 10, ToMinutes: 0, Name: "Тестовое событие 1"}
	updatedEvent2 := pkg.ScheduledEvent{ID: "test2", FromHours: 11, FromMinutes: 0, ToHours: 12, ToMinutes: 0, Name: "Тестовое событие 2"}
	updatedEvents = append(updatedEvents, updatedEvent2)
	repo.On("UpdateUserEvents", 10, updatedEvents, updateDate).Return(nil)
	syncedEvents, err := eventsHandler.Sync(requestEvents, 10, lastSyncDate, updateDate)
	if err != nil {
		log.Print("Got error while testing Sync: ", err)
		t.FailNow()
	}
	assert.Equal(t, requestEvents, syncedEvents, "input and response events are different")
}
