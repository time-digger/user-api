package internal

import (
	"time"
	"user-service/pkg"

	"google.golang.org/api/oauth2/v1"
)

//EventsHandler это интерфейс, соответствующий струтуре EventsHandlerImpl
type EventsHandler interface {
	Sync(syncEvents []pkg.ScheduledEvent, userID int, lastSyncDate time.Time, updateDate time.Time) (syncedEvents []pkg.ScheduledEvent, err error)
}

//AuthHandler это интерфейс, соответствующий структуре AuthHandlerImpl
type AuthHandler interface {
	Auth(idToken string) (userID int, userToken string, tokenInfo *oauth2.Tokeninfo, err error)
	ValidateToken(tokenString string) (userID int, valid bool, err error)
}
