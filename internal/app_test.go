package internal

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"testing"
	"user-service/pkg"

	"github.com/stretchr/testify/assert"
)

func TestSyncWithoutToken(t *testing.T) {
	eventsHandler := new(MockedEventsHandler)
	authHandler := new(MockedAuthHandler)

	app := NewApp(eventsHandler, authHandler)
	r := app.setupRouter()

	requestBody := []byte(`
	{
		"userId": 10,
		"scheduledEvents": [
			{
				"id": "asfaf",
				"fromHours": 121,
				"fromMinutes": 0,
				"toHours": 11,
				"toMinutes": 0,
				"name": "kakat"
			},
			{
				"id": "2",
				"fromHours": 10,
				"fromMinutes": 0,
				"toHours": 11,
				"toMinutes": 0,
				"name": "kakat hochu2223"
			}
		],
		"lastSyncDate": "2022-01-02T15:04:05Z"
	}
	`)

	w := performRequest(t, r, "POST", "/sync", requestBody)
	assert.Equal(t, http.StatusForbidden, w.Code)
	var response map[string]string
	err := json.Unmarshal([]byte(w.Body.String()), &response)
	assert.NoError(t, err, "could not parse response body")
	value, exists := response["description"]
	// Make some assertions on the correctness of the response.
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, "request has no token", value)
}

func TestSyncSuccessfulAuth(t *testing.T) {
	eventsHandler := new(MockedEventsHandler)
	authHandler := new(MockedAuthHandler)

	app := NewApp(eventsHandler, authHandler)
	r := app.setupRouter()

	requestBody := []byte(`
	{
		"userId": 10,
		"scheduledEvents": [
			{
				"id": "asfaf",
				"fromHours": 121,
				"fromMinutes": 0,
				"toHours": 11,
				"toMinutes": 0,
				"name": "kakat"
			},
			{
				"id": "2",
				"fromHours": 10,
				"fromMinutes": 0,
				"toHours": 11,
				"toMinutes": 0,
				"name": "kakat hochu2223"
			}
		],
		"lastSyncDate": "2022-01-02T15:04:05Z"
	}
	`)
	authHandler.On("ValidateToken", "Bearer fake-token").Return(10, true, nil)
	eventsHandler.On("Sync", mock.AnythingOfType("[]pkg.ScheduledEvent"), 10, mock.AnythingOfType("time.Time"), mock.AnythingOfType("time.Time")).Return(make([]pkg.ScheduledEvent, 0), nil)

	w := performRequestWithToken(t, r, "POST", "/sync", requestBody, "fake-token")
	assert.Equal(t, http.StatusOK, w.Code)
}

func performRequest(t *testing.T, r http.Handler, method, path string, jsonStrBody []byte) *httptest.ResponseRecorder {
	requestBody := bytes.NewBuffer(jsonStrBody)
	t.Log("Got request body: ", requestBody)
	req, _ := http.NewRequest(method, path, requestBody)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func performRequestWithToken(t *testing.T, r http.Handler, method, path string, jsonStrBody []byte, token string) *httptest.ResponseRecorder {
	requestBody := bytes.NewBuffer(jsonStrBody)
	t.Log("Got request body: ", requestBody)
	req, _ := http.NewRequest(method, path, requestBody)
	req.Header.Set("Authorization", "Bearer "+token)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}
