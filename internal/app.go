package internal

import (
	"user-service/pkg"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	log "github.com/sirupsen/logrus"
)

//App это ключевая структура для управления приложением
type App struct {
	eventsHandler EventsHandler
	authHandler   AuthHandler
}

//NewApp инициализирует App
func NewApp(eventsHandler EventsHandler, authHandler AuthHandler) *App {
	return &App{
		eventsHandler: eventsHandler,
		authHandler:   authHandler,
	}
}

//Run запускает HTTP-эндпоинты
func (a *App) Run() {
	r := a.setupRouter()
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

//ProcessSyncRequest обрабатывает запросы синхронизации
func (a *App) processSyncRequest() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Info("Got sync request")
		var r pkg.SyncRequest
		err := c.ShouldBindBodyWith(&r, binding.JSON)
		if err != nil {
			log.Println("Could not read request body", err)
			c.JSON(400, gin.H{
				"status": "error",
			})
			return
		}
		log.Infof("Request structure: %+v", r)

		se, err := a.eventsHandler.Sync(r.ScheduledEvents, r.UserID, r.LastSyncDate, r.UpdateDate)
		if err != nil {
			log.Error("Could not sync events")
			c.JSON(500, gin.H{
				"status": "error",
			})
			return
		}
		log.Infof("Got synced events: %+v", se)
		c.JSON(200, se)
	}
}

//ProcessAuthRequest обрабатывает запросы аутентификации
func (a *App) processAuthRequest() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Info("Got auth request")
		var r pkg.AuthRequest
		err := c.BindJSON(&r)
		if err != nil {
			log.Println("Could not read request body", err)
			c.JSON(400, gin.H{
				"status": "error",
			})
			return
		}

		userID, userToken, tokenInfo, err := a.authHandler.Auth(r.GoogleIDToken)
		if err != nil {
			c.JSON(500, gin.H{"status": "Error", "description": err})
		}

		c.JSON(200, gin.H{
			"status":    "OK",
			"userId":    userID,
			"userToken": userToken,
			"tokenInfo": tokenInfo,
		})
	}
}

//AuthRequired - middleware для запросов, которым необходимо пройти аутентификацию
func (a *App) authRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Info("Auth is required")
		var r pkg.UserRequest
		err := c.ShouldBindBodyWith(&r, binding.JSON)
		if err != nil {
			log.Error("Could not read request body for auth: ", err)
			c.JSON(400, gin.H{
				"status":      "error",
				"description": err,
			})
			c.Abort()
			return
		}

		tokenSlice, found := c.Request.Header["Authorization"]
		if !found || tokenSlice[0] == "" {
			log.Error("Auth token was not provided")
			c.JSON(403, gin.H{
				"status":      "error",
				"description": "request has no token",
			})
			c.Abort()
			return
		}

		tokenUserId, valid, err := a.authHandler.ValidateToken(tokenSlice[0])
		if err != nil {
			log.Error("Got error while validating token: ", err)
			c.JSON(400, gin.H{
				"status":      "error",
				"description": err,
			})
			c.Abort()
			return
		} else if !valid {
			log.Error("Auth token is not valid")
			c.JSON(403, gin.H{
				"status":      "error",
				"description": "auth token is not valid",
			})
			c.Abort()
			return
		}
		log.Infof("UserID in request: %v", r.UserID)
		log.Infof("UserId in token: %v", tokenUserId)

		if tokenUserId != r.UserID{
			log.Error("UserID in token is not valid")
			c.JSON(403, gin.H{
				"status":      "error",
				"description": "userId in token is not valid",
			})
			c.Abort()
			return
		}
	}
}

func (a *App) setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.POST("/auth", a.processAuthRequest())

	secured := r.Group("/")
	secured.Use(a.authRequired())
	secured.POST("/sync", a.processSyncRequest())

	return r
}
