package internal

import (
	"time"
	pkg "user-service/pkg"

	log "github.com/sirupsen/logrus"
)

//EventsHandlerImpl это структура, управляющая событиями пользователя
type EventsHandlerImpl struct {
	userRepository pkg.UserRepository
}

//NewEventsHandler инициализирует EventsHandler
func NewEventsHandler(userRepository pkg.UserRepository) *EventsHandlerImpl {
	return &EventsHandlerImpl{userRepository: userRepository}
}

//Sync синхронизирует совмещает события пользователя, пришедшие на вход и находяищиеся в БД
//На выходе дает финальный срез синхронизированных событий
func (h *EventsHandlerImpl) Sync(syncEvents []pkg.ScheduledEvent, userID int, lastSyncDate time.Time, updateDate time.Time) (syncedEvents []pkg.ScheduledEvent, err error) {
	dbEvents, err := h.userRepository.GetUserEvents(userID)
	if err != nil {
		log.Error("Could not get user events: ", err)
		return nil, err
	}
	log.Infof("Got events from db: %+v", dbEvents)
	resultMap := mapEvents(syncEvents)
	for _, e := range dbEvents {
		_, found := resultMap[e.ID]
		if !found {
			log.Infof("Could not find event with id %v in sync request", e.ID)
			//Проверка, было ли событие удалено в приложении
			if e.UpdateDate.Before(lastSyncDate) || e.UpdateDate.Equal(lastSyncDate) {
				log.Infof("Event %v was deleted in application", e.ID)
				e.Deleted = true
			}
			// В любом случае, добавляем событие в общую мапу
			resultMap[e.ID] = e
		} else {
			//Проверка, отличаются ли версии события в бд и в запросе на синхронизацию и не было ли удалено событие в бд
			if lastSyncDate.Before(e.UpdateDate) || e.Deleted {
				resultMap[e.ID] = e
			}
		}
	}

	var resultSlice []pkg.ScheduledEvent
	for _, val := range resultMap {
		resultSlice = append(resultSlice, val)
	}
	log.Infof("Got result slice: %+v", resultSlice)

	if err = h.userRepository.UpdateUserEvents(userID, resultSlice, updateDate); err != nil {
		log.Error("Could not update DB events: ", err)
		return
	}

	syncedEvents = make([]pkg.ScheduledEvent, 0, len(resultSlice))
	for _, e := range resultSlice {
		if !e.Deleted {
			syncedEvents = append(syncedEvents, e)
		}
	}
	return
}

func mapEvents(se []pkg.ScheduledEvent) map[string]pkg.ScheduledEvent {
	m := make(map[string]pkg.ScheduledEvent)
	for _, e := range se {
		m[e.ID] = e
	}
	return m
}
