package internal

import (
	"testing"
	"user-service/pkg"
	"user-service/pkg/postgresql"

	"github.com/stretchr/testify/assert"
	"google.golang.org/api/oauth2/v1"
)

func TestAuthNewUser(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)
	oauth := new(pkg.MockedOauthClient)
	signingKey := []byte("test-signing-key")
	authHandler := NewAuthHandler(repo, oauth, signingKey)

	idToken := "test-idtoken"

	tokenInfo := &oauth2.Tokeninfo{Email: "test@mail.com", UserId: "testUserId"}
	oauth.On("VerifyIDToken", idToken).Return(tokenInfo, nil)
	repo.On("FindUser", tokenInfo.Email).Return(0, false, nil)
	repo.On("AddUser", tokenInfo.Email, tokenInfo.UserId).Return(10, nil)
	userID, userToken, responseTokenInfo, err := authHandler.Auth(idToken)
	assert.Equal(t, 10, userID, "userID is wrong")
	assert.Equal(t, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOjEwfQ.5nhM-2Pbx0b3ecHt-umYJnvOpRgCST6XLNv4rSPv_3M", userToken, "tokenInfo is wrong")
	assert.Equal(t, tokenInfo, responseTokenInfo, "tokenInfo is wrong")
	assert.NoError(t, err, "Auth returned error")
}

func TestValidateToken(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)
	oauth := new(pkg.MockedOauthClient)
	signingKey := []byte("test-signing-key")
	authHandler := NewAuthHandler(repo, oauth, signingKey)
	userToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOjEwfQ.5nhM-2Pbx0b3ecHt-umYJnvOpRgCST6XLNv4rSPv_3M"
	userID, valid, err := authHandler.ValidateToken(userToken)
	assert.NoError(t, err, "Validate returned error")
	assert.Equal(t, 10, userID, "userID is wrong")
	assert.Equal(t, true, valid, "token is not valid")
}

func TestValidateTokenWronguserID(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)
	oauth := new(pkg.MockedOauthClient)
	signingKey := []byte("test-signing-key")
	authHandler := NewAuthHandler(repo, oauth, signingKey)
	userToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOjQyfQ.xQcHZuuuGM_5k7Fn7AL5AKCSzJ4peucP_0jCtsSpheE"
	userID, valid, err := authHandler.ValidateToken(userToken)
	assert.NoError(t, err, "Validate returned error")
	assert.NotEqual(t, 10, userID, "userID is wrong")
	assert.Equal(t, true, valid, "token is not valid")
}

func TestValidateTokenInvalidSigningKey(t *testing.T) {
	repo := new(postgresql.MockedUserRepository)
	oauth := new(pkg.MockedOauthClient)
	signingKey := []byte("test-signing-key")
	authHandler := NewAuthHandler(repo, oauth, signingKey)
	userToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOjEwfQ.wmQ6p464cpeJIdUBc74YL4jnwyndy33PEjkp6PgQUQ4"
	_, valid, err := authHandler.ValidateToken(userToken)
	assert.Error(t, err, "Validate returned error")
	assert.Equal(t, false, valid, "token is valid but should not be")
}
