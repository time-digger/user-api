package internal

import (
	"time"
	"user-service/pkg"

	"github.com/stretchr/testify/mock"
)

// MockedEventsHandler это мок для EventsHandler
type MockedEventsHandler struct {
	mock.Mock
}

// Sync это замоканный синк
func (h *MockedEventsHandler) Sync(syncEvents []pkg.ScheduledEvent, userID int, lastSyncDate time.Time, updateDate time.Time) (syncedEvents []pkg.ScheduledEvent, err error) {
	args := h.Called(syncEvents, userID, lastSyncDate, updateDate)
	return args.Get(0).([]pkg.ScheduledEvent), args.Error(1)
}
