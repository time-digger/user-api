package internal

import (
	"errors"
	"user-service/pkg"

	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
	"google.golang.org/api/oauth2/v1"
)

//AuthHandlerImpl это структура, управляющая аутентификацией пользователей
type AuthHandlerImpl struct {
	userRepository pkg.UserRepository
	oauthClient    pkg.OauthClient
	signingKey     []byte
}

//NewAuthHandler - конструктор для AuthHandler
func NewAuthHandler(userRepository pkg.UserRepository, oauthClient pkg.OauthClient, signingKey []byte) *AuthHandlerImpl {
	return &AuthHandlerImpl{userRepository: userRepository, oauthClient: oauthClient, signingKey: signingKey}
}

//Auth аутентифицирует токен и регистирует новго пользователя при необходимости
func (h *AuthHandlerImpl) Auth(idToken string) (userID int, userToken string, tokenInfo *oauth2.Tokeninfo, err error) {
	// tokenInfo, err = verifyIDToken(h.httpClient, idToken)
	tokenInfo, err = h.oauthClient.VerifyIDToken(idToken)
	if err != nil {
		log.Error("Got error while trying to verify token: ", err)
		return
	}

	userID, found, err := h.userRepository.FindUser(tokenInfo.Email)
	if err != nil {
		log.Error("Could not find user in DB: ", err)
		return
	}

	if !found {
		userID, err = h.userRepository.AddUser(tokenInfo.Email, tokenInfo.UserId)
		if err != nil {
			log.Error("Could not add user to DB: ", err)
			return
		}
	}

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["userID"] = userID
	userToken, err = token.SignedString(h.signingKey)
	if err != nil {
		log.Error("Could not get string jwt: ", err)
		return
	}

	return
}

// ValidateToken проверяет валидность токена
func (h *AuthHandlerImpl) ValidateToken(tokenString string) (userID int, valid bool, err error) {
	log.Info("Validating token")
	// Parse takes the token string and a function for looking up the key. The latter is especially
	// useful if you use multiple keys for your application.  The standard is to use 'kid' in the
	// head of the token to identify which key to use, but the parsed token (head and claims) is provided
	// to the callback, providing flexibility.
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			log.Errorf("Unexpected signing method: %v", token.Header["alg"])
			return nil, errors.New("unexpected signing method")
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return h.signingKey, nil
	})
	if err != nil {
		log.Error("Could not parse token: ", err)
		valid = false
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		userID = int(claims["userID"].(float64))
		log.Info("Token is valid")
		log.Infof("userId in token: %v", userID)
		valid = true
	} else {
		log.Error("Token is invalid")
		valid = false
	}
	return
}
