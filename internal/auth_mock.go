package internal

import (
	"github.com/stretchr/testify/mock"
	"google.golang.org/api/oauth2/v1"
)

//MockedAuthHandler это мок для AuthHandler
type MockedAuthHandler struct {
	mock.Mock
}

//Auth это замоканный Auth
func (h *MockedAuthHandler) Auth(idToken string) (userID int, userToken string, tokenInfo *oauth2.Tokeninfo, err error) {
	args := h.Called(idToken)
	return args.Int(0), args.String(1), args.Get(2).(*oauth2.Tokeninfo), args.Error(3)
}

func (h *MockedAuthHandler) ValidateToken(tokenString string) (userID int, valid bool, err error) {
	args := h.Called(tokenString)
	return args.Int(0), args.Bool(1), args.Error(2)
}
