package main

import (
	"net/http"
	"os"

	internal "user-service/internal"
	"user-service/pkg"
	postgresql "user-service/pkg/postgresql"

	"github.com/jmoiron/sqlx"
	_ "github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

func init() {
	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "2006-01-02T15:04:05.999999999Z07:00"
	Formatter.FullTimestamp = true
	Formatter.ForceColors = true
	log.SetFormatter(Formatter)
	log.SetLevel(log.DebugLevel)
}

func main() {
	db := initDb()
	var userRepository pkg.UserRepository
	userRepository = postgresql.NewUserRepository(db)
	eventsHandler := internal.NewEventsHandler(userRepository)
	var httpClient = &http.Client{}
	signingKey := []byte(os.Getenv("SIGNING_KEY"))
	if len(signingKey) == 0 {
		log.Error("Signing key is empty")
		panic("signing key is empty")
	}
	oauthClient := pkg.NewOauthClient(httpClient)
	authHandler := internal.NewAuthHandler(userRepository, oauthClient, signingKey)
	app := internal.NewApp(eventsHandler, authHandler)
	app.Run()
}

func initDb() *sqlx.DB {
	connStr := os.Getenv("POSTGRES_CON_STR")
	// connStr := "postgres://postgres:Gb6kF8h7@localhost/users?sslmode=disable"
	var err error
	// db, err = sql.Open("postgres", connStr)
	db, err := sqlx.Connect("postgres", connStr)
	if err != nil {
		log.Error("Could not open DB connection", err)
		panic(err)
	}
	if err = db.Ping(); err != nil {
		log.Error("Could not open DB connection", err)
		panic(err)
	}

	return db
}
