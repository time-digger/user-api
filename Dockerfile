FROM golang:alpine as builder
WORKDIR /app
COPY go.mod go.sum ./
COPY . .
RUN cd cmd && go build -o main

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /app/cmd/main .
EXPOSE 8080
CMD ["./main"]