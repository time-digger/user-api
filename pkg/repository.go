package pkg

import (
	"time"
	
)

// UserRepository это интерфейс, соответствующий репозиторию, возвращающем и изменяющему информацию о пользователе
type UserRepository interface {
	AddUser(email, googleUserID string) (userID int, err error)
	FindUser(email string) (userID int, found bool, err error)
	UpdateUserEvents(userID int, se []ScheduledEvent, updateDate time.Time) error
	GetUserEvents(userID int) ([]ScheduledEvent, error)
}
