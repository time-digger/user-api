package postgresql

import (
	"time"
	"user-service/pkg"

	"github.com/stretchr/testify/mock"
)

//MockedUserRepository соответствует замоканному репозиторию данных пользователя
type MockedUserRepository struct {
	mock.Mock
}

//AddUser добавляет новго пользователя в БД
func (r *MockedUserRepository) AddUser(email, googleUserID string) (userID int, err error) {
	args := r.Called(email, googleUserID)
	return args.Int(0), args.Error(1)
}

//FindUser возвращает пользовательские данные если пользователь существует
//Если пользователь не найден, переменная bool будет true
func (r *MockedUserRepository) FindUser(email string) (userID int, found bool, err error) {
	args := r.Called(email)
	return args.Int(0), args.Bool(1), args.Error(2)
}

//GetUserEvents возвращает все события пользователя из бд
func (r *MockedUserRepository) GetUserEvents(userID int) ([]pkg.ScheduledEvent, error) {
	args := r.Called(userID)
	return args.Get(0).([]pkg.ScheduledEvent), args.Error(1)
}

//UpdateUserEvents обновляет события пользователя в бд
func (r *MockedUserRepository) UpdateUserEvents(userID int, se []pkg.ScheduledEvent, updateDate time.Time) error {
	args := r.Called(userID, se, updateDate)
	return args.Error(0)
}
