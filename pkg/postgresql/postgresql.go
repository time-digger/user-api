package postgresql

import (
	"database/sql"
	"errors"
	"time"
	"user-service/pkg"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
)

//UserRepository соответствует репозиторию данных пользователя
type UserRepository struct {
	db *sqlx.DB
}

//NewUserRepository инициализирует UserRepository
func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{db: db}
}

//AddUser добавляет новго пользователя в БД
func (r *UserRepository) AddUser(email, googleUserID string) (userID int, err error) {
	sql := "insert into users (email, google_user_id) values ($1, $2) returning id"
	row := r.db.QueryRow(sql, email, googleUserID)
	if err = row.Scan(&userID); err != nil {
		log.Error("Could not add user to DB: ", err)
		return
	}
	return
}

//FindUser возвращает пользовательские данные если пользователь существует
//Если пользователь не найден, переменная bool будет true
func (r *UserRepository) FindUser(email string) (userID int, found bool, err error) {
	query := "SELECT id, token FROM users where email = $1"
	row := r.db.QueryRow(query, email)
	err = row.Scan(&userID)
	if errors.Is(err, sql.ErrNoRows) {
		found = false
		err = nil
		return
	}
	found = true
	return
}

//GetUserEvents возвращает все события пользователя из бд
func (r *UserRepository) GetUserEvents(userID int) ([]pkg.ScheduledEvent, error) {
	query := "SELECT id, from_hours, from_minutes, to_hours, to_minutes, name, update_date, deleted from scheduled_events where user_id = $1"
	rows, err := r.db.Queryx(query, userID)
	if err != nil {
		log.Error("Could not execute user events query: ", err)
		return nil, err
	}
	defer rows.Close()

	dbEvents := make([]pkg.ScheduledEvent, 0)
	for rows.Next() {
		dbEvent := pkg.ScheduledEvent{}
		err = rows.StructScan(&dbEvent)
		if err != nil {
			log.Error("Could not call StructScan for scheduled events: ", err)
			return nil, err
		}
		dbEvents = append(dbEvents, dbEvent)
	}
	return dbEvents, nil
}

//UpdateUserEvents обновляет события пользователя в бд
func (r *UserRepository) UpdateUserEvents(userID int, se []pkg.ScheduledEvent, updateDate time.Time) error {
	query := `
		insert
			into scheduled_events (id, from_hours, from_minutes, to_hours, to_minutes, "name", user_id, update_date, deleted)
			values ($1, $2, $3, $4,	$5, $6, $7, $8, $9) 
		on conflict (id) do
			update
			set
				from_hours = $2,
				from_minutes = $3,
				to_hours = $4,
				to_minutes = $5,
				"name" = $6,
				user_id = $7,
				update_date = $8,
				deleted = $9
	`
	for _, e := range se {
		_, err := r.db.Exec(query, e.ID, e.FromHours, e.FromMinutes, e.ToHours, e.ToMinutes, e.Name, userID, updateDate, e.Deleted)
		if err != nil {
			log.Error("Could not sync events in DB: ", err)
			return err
		}
	}
	return nil
}
