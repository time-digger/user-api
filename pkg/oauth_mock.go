package pkg

import (
	"github.com/stretchr/testify/mock"
	"google.golang.org/api/oauth2/v1"
)

// MockedOauthClient это замоканный OauthClient
type MockedOauthClient struct {
	mock.Mock
}

// VerifyIDToken это замоканный вызов VerifyIDToken
func (c *MockedOauthClient) VerifyIDToken(idToken string) (*oauth2.Tokeninfo, error) {
	args := c.Called(idToken)
	return args.Get(0).(*oauth2.Tokeninfo), args.Error(1)
}
