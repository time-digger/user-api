package pkg

import "time"

//ScheduledEvent это структура, соответствующая запланированному событию
type ScheduledEvent struct {
	ID          string    `db:"id" json:"id"`
	FromHours   int       `db:"from_hours" json:"fromHours"`
	FromMinutes int       `db:"from_minutes" json:"fromMinutes"`
	ToHours     int       `db:"to_hours" json:"toHours"`
	ToMinutes   int       `db:"to_minutes" json:"toMinutes"`
	Name        string    `db:"name" json:"name"`
	UpdateDate  time.Time `db:"update_date" json:"-"`
	Deleted     bool      `db:"deleted" json:"-"`
}

//AuthRequest это структура соответствующая запросу на авторизацию
type AuthRequest struct {
	GoogleIDToken string `json:"googleIdToken"`
}

//SyncRequest это структура соответствубющая запросу на синхронизацию
type SyncRequest struct {
	UserRequest
	// UserID          int              `json:"userId"`
	ScheduledEvents []ScheduledEvent `json:"scheduledEvents"`
	LastSyncDate    time.Time        `json:"lastSyncDate"`
	UpdateDate      time.Time        `json:"updateDate"`
}

// UserRequest это структура, встраиваемая в структуры запросов, для которых нужна аутентификация
type UserRequest struct {
	UserID int `json:"userId"`
}
