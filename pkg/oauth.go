package pkg

import (
	"net/http"

	"google.golang.org/api/oauth2/v1"
)

// OauthClient это интерфейс для OauthClientImpl
type OauthClient interface {
	VerifyIDToken(idToken string) (*oauth2.Tokeninfo, error)
}

// OauthClientImpl это клиент для верификации google токенов
type OauthClientImpl struct {
	httpClient *http.Client
}

// NewOauthClient создает новый инстанс OauthClientImpl
func NewOauthClient(httpClient *http.Client) *OauthClientImpl {
	return &OauthClientImpl{httpClient: httpClient}
}

// VerifyIDToken производит проверку id token от google,
// после чего возвращает информацию о пользователе, которому этот токен принадлежит
func (c *OauthClientImpl) VerifyIDToken(idToken string) (*oauth2.Tokeninfo, error) {
	oauth2Service, err := oauth2.New(c.httpClient)
	tokenInfoCall := oauth2Service.Tokeninfo()
	tokenInfoCall.IdToken(idToken)
	tokenInfo, err := tokenInfoCall.Do()
	if err != nil {
		return nil, err
	}
	return tokenInfo, nil
}
